from users import User


def test_user_initial_state():
    test_user = User("test@example.com", "Passw0rd")
    assert test_user.email == "test@example.com"
    assert test_user.password == "Passw0rd"
    assert not test_user.logged_in


def test_successful_login():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("Passw0rd")
    assert test_user.logged_in


def test_unsuccessful_login():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("WrongPass123")
    assert not test_user.logged_in


def test_logout():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("Passw0rd")
    test_user.logout()
    assert not test_user.logged_in
